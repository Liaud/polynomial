TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.c \
    utils/list.c \
    polynomial/monomial.c \
    polynomial/polynomial.c \
    polynomial/polynomial_root.c \
    polynomial/operations.c \
    utils/complex.c

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    utils/bool.h \
    utils/list.h \
    polynomial/monomial.h \
    polynomial/polynomial.h \
    polynomial/polynomial_root.h \
    polynomial/operations.h \
    utils/complex.h

