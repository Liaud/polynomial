#include <stdlib.h>
#include "polynomial/polynomial.h"
#include "polynomial/operations.h"

int main()
{
    double coefficients[] =
    {
        0, 1, 5, 6, -7
    };
    double coefficients2[] =
    {
        0, 1, -5, 6, 0
    };
    Polynomial poly1 = Polynomial_createWithCoefficents(5, coefficients);
    Polynomial poly2 = Polynomial_createWithCoefficents(5, coefficients2);

    Polynomial_add(&poly1, &poly2);
    //Polynomial_subtract(&poly1, &poly2);
    Polynomial_display(&poly1);

    Polynomial_destroy(&poly1);
    Polynomial_destroy(&poly2);
    return 0;
}
