#include "polynomial/monomial.h"

#include <stdlib.h>

Monomial* Monomial_create(double factor, int exponant)
{
    Monomial* monomial = (Monomial*)malloc(sizeof(Monomial));
    monomial->node = ListNode_create();

    monomial->factor = factor;
    monomial->exponant = exponant;

    return monomial;
}

Monomial* Monomial_copy(Monomial* other)
{
    return Monomial_create(other->factor, other->exponant);
}

void Monomial_destroy(Monomial* monomial)
{
    free(monomial);
}

void Monomial_freeHandler(ListNode* monomial)
{
    Monomial_destroy((Monomial*)monomial);
}
