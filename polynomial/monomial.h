#ifndef MONOMIAL_H
#define MONOMIAL_H

#include "utils/list.h"

typedef struct Monomial
{
    ListNode node;

    double factor;
    int exponant;
} Monomial;

Monomial* Monomial_create(double factor, int exponant);
Monomial* Monomial_copy(Monomial* other);
void Monomial_destroy(Monomial* monomial);
void Monomial_freeHandler(ListNode* monomial);

#endif // MONOMIAL_H
