#include "polynomial/operations.h"

#include <stdlib.h>

Polynomial* Polynomial_add(Polynomial* left, Polynomial* right)
{
    MonomialList result = List_create(Monomial_freeHandler);

    Monomial* left_current = LIST_BEGIN_AS(Monomial*, left->monomials);
    Monomial* right_current = LIST_BEGIN_AS(Monomial*, right->monomials);
    while(left_current != NULL && right_current != NULL)
    {
        if(left_current->exponant == right_current->exponant)
        {
            left_current->factor += right_current->factor;

            LIST_APPEND_LAST(result, Monomial_copy(left_current));

            LIST_ADVANCE_AS(Monomial*, left_current);
            LIST_ADVANCE_AS(Monomial*, right_current);
        }
        else if(left_current->exponant > right_current->exponant)
        {
            LIST_APPEND_LAST(result, Monomial_copy(right_current));

            LIST_ADVANCE_AS(Monomial*, right_current);
        }
        else
        {
            LIST_APPEND_LAST(result, Monomial_copy(left_current));

            LIST_ADVANCE_AS(Monomial*, left_current);
        }
    }

    while(left_current != NULL)
    {
        LIST_APPEND_LAST(result, Monomial_copy(left_current));
        LIST_ADVANCE_AS(Monomial*, left_current);
    }

    while(right_current != NULL)
    {
        LIST_APPEND_LAST(result, Monomial_copy(right_current));
        LIST_ADVANCE_AS(Monomial*, right_current);
    }

    List_clear(&left->monomials);
    left->monomials = result;
    return left;
}

Polynomial* Polynomial_subtract(Polynomial* left, Polynomial* right)
{
    MonomialList result = List_create(Monomial_freeHandler);
    Monomial* subtracted = NULL;

    Monomial* left_current = LIST_BEGIN_AS(Monomial*, left->monomials);
    Monomial* right_current = LIST_BEGIN_AS(Monomial*, right->monomials);
    while(left_current != NULL && right_current != NULL)
    {
        if(left_current->exponant == right_current->exponant)
        {
            double subtracted_factor = left_current->factor - right_current->factor;
            if(subtracted_factor != 0)
            {
                left_current->factor = subtracted_factor;
                LIST_APPEND_LAST(result, Monomial_copy(left_current));
            }

            LIST_ADVANCE_AS(Monomial*, left_current);
            LIST_ADVANCE_AS(Monomial*, right_current);
        }
        else if(left_current->exponant > right_current->exponant)
        {
            subtracted = Monomial_copy(right_current);
            subtracted->factor *= -1;

            LIST_APPEND_LAST(result, subtracted);

            LIST_ADVANCE_AS(Monomial*, right_current);
        }
        else
        {
            subtracted = Monomial_copy(left_current);
            subtracted->factor *= -1;

            LIST_APPEND_LAST(result, subtracted);
            LIST_ADVANCE_AS(Monomial*, left_current);
        }
    }

    while(left_current != NULL)
    {
        subtracted = Monomial_copy(left_current);
        subtracted->factor *= -1;

        LIST_APPEND_LAST(result, subtracted);
        LIST_ADVANCE_AS(Monomial*, left_current);
    }

    while(right_current != NULL)
    {
        subtracted = Monomial_copy(right_current);
        subtracted->factor *= -1;

        LIST_APPEND_LAST(result, subtracted);
        LIST_ADVANCE_AS(Monomial*, right_current);
    }

    List_clear(&left->monomials);
    left->monomials = result;
    return left;
}
