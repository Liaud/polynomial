#ifndef OPERATIONS_H
#define OPERATIONS_H

#include "polynomial.h"

Polynomial* Polynomial_add(Polynomial* left, Polynomial* right);
Polynomial* Polynomial_subtract(Polynomial* left, Polynomial* right);

#endif // OPERATIONS_H
