#include "polynomial/polynomial.h"
#include "utils/bool.h"

#include <stdio.h>

inline Polynomial Polynomial_create()
{
    Polynomial polynomial;
    polynomial.monomials = List_create(Monomial_freeHandler);
    polynomial.roots = List_create(PolynomialRoot_freeHandler);

    return polynomial;
}

Polynomial Polynomial_createWithCoefficents(int polynomial_degree, double coefficients[])
{
    Polynomial polynomial = Polynomial_create();

    int i;
    for(i = 0; i < polynomial_degree; ++i)
    {
        Monomial* monomial = Monomial_create(coefficients[i], i);
        List_appendLast(&polynomial.monomials, (ListNode*)monomial);
    }

    return polynomial;
}

Polynomial Polynomial_copy(Polynomial *other)
{
    Polynomial polynomial = Polynomial_create();

    Monomial* current_monomial = (Monomial*)other->monomials.head;
    while(current_monomial != NULL)
    {
        List_appendLast(&polynomial.monomials, (ListNode*)Monomial_copy(current_monomial));
    }

    PolynomialRoot* current_root = (PolynomialRoot*)other->roots.head;
    while(current_root != NULL)
    {
        List_appendLast(&polynomial.roots, (ListNode*)PolynomialRoot_copy(current_root));
    }

    return polynomial;
}

void Polynomial_display(Polynomial* polynomial)
{
    printf("==== Polynome ====\n");
    printf("Forme developpee:\n\t\n");

    Monomial* current_monomial = (Monomial*)polynomial->monomials.head;
    while(current_monomial != NULL)
    {
        if(current_monomial->factor != 0)
        {
            printf(" %.1fx^%d +", current_monomial->factor, current_monomial->exponant);
        }

        current_monomial = (Monomial*)current_monomial->node.next;
    }

    printf("\n\n");
    printf("Forme factorise: \n\t");

    PolynomialRoot* current_root = (PolynomialRoot*)polynomial->roots.head;
    while(current_root != NULL)
    {
        printf(
            "%.3f(x - %d)^%d * ",
            current_root->factor,
            current_root->value,
            current_root->multiplicity
        );

        current_root = (PolynomialRoot*)current_root->node.next;
    }
}

void Polynomial_destroy(Polynomial* polynomial)
{
    List_clear(&polynomial->monomials);
    List_clear(&polynomial->roots);
}
