#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

#include "utils/list.h"
#include "monomial.h"
#include "polynomial_root.h"

typedef List MonomialList;
typedef List PolynomialRootList;

typedef struct Polynomial
{
    MonomialList monomials;
    PolynomialRootList roots;
} Polynomial;

Polynomial Polynomial_create();
Polynomial Polynomial_createWithCoefficents(int polynomial_degree, double coefficients[]);
Polynomial Polynomial_copy(Polynomial* other);
void Polynomial_display(Polynomial* polynomial);
void Polynomial_destroy(Polynomial* polynomial);


#endif // POLYNOMIAL_H
