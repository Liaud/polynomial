#include "polynomial/polynomial_root.h"

#include <stdlib.h>

PolynomialRoot* PolynomialRoot_create(double factor, int multiplicity, int value)
{
    PolynomialRoot* root = (PolynomialRoot*)malloc(sizeof(PolynomialRoot));
    root->node = ListNode_create();

    root->factor = factor;
    root->multiplicity = multiplicity;
    root->value = value;

    return root;
}

PolynomialRoot* PolynomialRoot_copy(PolynomialRoot* other)
{
    return PolynomialRoot_create(
        other->factor,
        other->multiplicity,
        other->value
    );
}

void PolynomialRoot_destroy(PolynomialRoot *root)
{
    free(root);
}

void PolynomialRoot_freeHandler(ListNode* root)
{
    PolynomialRoot_destroy((PolynomialRoot*)root);
}
