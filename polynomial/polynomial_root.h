#ifndef POLYNOMIAL_ROOT_H
#define POLYNOMIAL_ROOT_H

#include "utils/list.h"

typedef struct PolynomialRoot
{
    ListNode node;

    double factor;
    int multiplicity;
    int value;
} PolynomialRoot;

PolynomialRoot* PolynomialRoot_create(double factor, int multiplicity, int value);
PolynomialRoot* PolynomialRoot_copy(PolynomialRoot* other);
void PolynomialRoot_destroy(PolynomialRoot* root);
void PolynomialRoot_freeHandler(ListNode* root);

#endif // POLYNOMIAL_ROOT_H
