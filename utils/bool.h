#ifndef BOOL_H
#define BOOL_H

typedef enum Bool
{
    False = 0,
    True
} Bool;

#endif // BOOL_H
