#include "complex.h"

#include <math.h>

Complex Complex_create(double real, double imaginary)
{
    Complex complex;
    complex.real = real;
    complex.imaginary = imaginary;

    return complex;
}


void Complex_display(Complex* complex)
{
    printf("%lf + %lfi", complex->real, complex->imaginary);
}

Complex* Complex_add(Complex* left, Complex* right)
{
    left->real += right->real;
    left->imaginary += right->imaginary;

    return left;
}


Complex* Complex_subtract(Complex* left, Complex* right)
{
    left->real -= right.real;
    left->imaginary -= right->imaginary;

    return left;
}



Complex* Complex_multiply(Complex* left, Complex* right)
{
    Complex result;

    result.real = (left->real*right->real) - (left->imaginary*right->imaginary);
    result.imaginary = (left->imaginary*right->real) + (left->real*right->imaginary);

    left->real = result.real;
    left->imaginary = result.imaginary;

    return left;
}


Complex* Complex_divide(Complex* left, Complex* right)
{
    Complex result;

    double divider = right->real*right->real + right->imaginary*right->imaginary;
    result.real = (left->real*right->real + left->imaginary*right->imaginary)/divider;
    result.imaginary = (left->imaginary*right->real - left->real*right->imaginary)/divider;

    left->real = result.real;
    left->imaginary = result.imaginary;

    return left;
}

Complex* Complex_conjugate(Complex* complex)
{
    complex->imaginary *= -1;
    return complex;
}

Complex* Complex_opposite(Complex* complex)
{
    complex->real *= -1;
    complex->imaginary *= -1;

    return complex;
}


double Complex_module(Complex* complex)
{
    return sqrt(complex->real*complex->real + complex->imaginary*complex->imaginary);
}


double Complex_arg(Complex* complex)
{
    if(complex->real < 0 && complex->imaginary == 0)
    {
        return 2*atan(complex->imaginary/(complex->real + Complex_module(complex)));
    }

    return 2*atan(complex->imaginary/(complex->real + M_PI));
}
