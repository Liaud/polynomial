#ifndef COMPLEX_H_INCLUDED
#define COMPLEX_H_INCLUDED


typedef struct Complex
{
    double real;
    double imaginary;
} Complex;

Complex Complex_create(double real, double imaginary);
void Complex_display(Complex* complex);
Complex* Complex_add(Complex* left, Complex* right);
Complex* Complex_subtract(Complex* left, Complex* right);
Complex* Complex_multiply(Complex* left, Complex* right);
Complex* Complex_divide(Complex* left, Complex* right);
Complex* Complex_conjugate(Complex* complex);
Complex* Complex_opposite(Complex* complex);
double Complex_module(Complex* complex);
double Complex_arg(Complex* complex);

#endif // COMPLEX_H_INCLUDED
