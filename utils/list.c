#include "list.h"
#include "bool.h"

#include <stdlib.h>

ListNode ListNode_create()
{
    ListNode node;
    node.next = NULL;
    node.previous = NULL;

    return node;
}

List List_create(ListFreeHandler free_handler)
{
    List list;
    list.tail = NULL;
    list.head = NULL;
    list.len = 0;
    list.free_handler = free_handler;

    return list;
}


void List_appendLast(List* list, ListNode* node)
{
    if(list->len == 0)
    {
        list->tail = node;
        list->head = node;
    }
    else
    {
        list->tail->next = node;
        node->previous = list->tail;
        list->tail = node;
    }

    list->len += 1;
}

void List_appendWhen(List* list, ListNode* node, ListInsertCondHandler cond)
{
    ListNode* current = list->head;
    while(current != NULL)
    {
        if(cond(current, node) == True)
        {
            if(current->previous) {
                current->previous->next = node;
                node->previous = current->previous;
            }
            node->next = current;

            return;
        }

        current = current->next;
    }

    List_appendLast(list, node);
}

void List_remove(List* list, ListNode* node) {
    Bool found = False;

    if(list->head == node)
    {
        list->head = list->head->next;

        found = True;
    }
    else if(list->tail == node)
    {
        list->tail = list->tail->previous;
        list->tail->next = NULL;

        found = True;
    }
    else
    {
        ListNode* current;
        while(current != NULL) {
            if(current == node)
            {
                current->previous->next = current->next;
                current->next->previous = current->previous;

                found = True;
                break;
            }

            current = current->next;
        }
    }

    if(found == True)
    {
        list->len -= 1;
        list->free_handler(node);
    }
}

int List_len(List* list)
{
    return list->len;
}

void List_clear(List* list)
{
    ListNode* current = list->head;
    while(current != NULL)
    {
        ListNode* next = current->next;
        list->free_handler(current);

        current = next;
    }
    list->head = NULL;
    list->tail = NULL;
    list->len = 0;
}
