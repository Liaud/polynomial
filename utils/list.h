#ifndef LIST_H
#define LIST_H

#include "bool.h"

typedef struct ListNode
{
    struct ListNode* next;
    struct ListNode* previous;
} ListNode;

typedef void (*ListFreeHandler)(ListNode*);
typedef Bool (*ListInsertCondHandler)(ListNode*, ListNode*);

typedef struct List {
    ListNode* head;
    ListNode* tail;
    int len;

    ListFreeHandler free_handler;
} List;

ListNode ListNode_create();

List List_create(ListFreeHandler free_handler);
void List_appendLast(List* list, ListNode* node);
void List_appendWhen(List* list, ListNode* node, ListInsertCondHandler cond);
void List_remove(List* list, ListNode* node);
int List_len(List* list);
void List_clear(List* list);

#define LIST_ADVANCE_AS(type, iterator) \
    iterator = (type)iterator->node.next;

#define LIST_APPEND_LAST(list, node) \
    List_appendLast(&list, (ListNode*)node);

#define LIST_APPEND_WHEN(list, node, condition) \
    List_appendWhen(&list, (ListNode*)node, condition);

#define LIST_REMOVE(list, node) \
    List_remove(&list, (ListNode*)node);

#define LIST_BEGIN_AS(type, list) \
    (type)list.head;

#endif // LIST_H
